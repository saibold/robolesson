main = do
    eingabe <- getLine
    let tokens = words eingabe
    multiJson tokens


jsonString terminal anfangsZ zielZ = putStrLn $ "{\"terminal\":\"" ++ terminal ++ "\", \"anfangsZustand\":\"" ++ anfangsZ ++ "\", \"zielZustand\":\"" ++ zielZ ++ "\"}"

jsonString2 terminal anfangsZ zielZ = putStrLn $ "{\"terminal\":\"" ++ terminal ++ "\", \"anfangsZustand\":\"" ++ anfangsZ ++ "\", \"zielZustand\":\"" ++ zielZ ++ "\"},"


multiJson tokens = 
    if (length tokens) < 4
        then do
            jsonString (head tokens) (head (tail tokens)) (head (tail (tail tokens)))
        else do
            jsonString2 (head tokens) (head (tail tokens)) (head (tail (tail tokens)))
            multiJson $ tail $ tail $ tail tokens
-- a x1 x2
-- -> {"terminal":"a", "anfangsZustand":"x1", "zielZustand":"x2"}