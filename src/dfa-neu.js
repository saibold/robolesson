const gameField = document.querySelector('#blackboard')
const rightMenu = document.querySelector('#right-page')
const leftMenu = document.querySelector('#left-page')
let frischerZustand = 0
let dfaJson
let lastStateClicked
let uebergangPhase
let lineDrawn = []
let selectedTerminal
let currentlyShownMenu = 'Overview'

class DFA {
    /**
     * 
     * @param {[string]} alphabet 
     * @param {[string]} zustaende 
     * @param {string} startzustand 
     * @param {[string]} endzustaende 
     * @param {[Uebergang]} uebergangsFunktion 
     */
    constructor(alphabet, zustaende, startzustand, endzustaende, uebergangsFunktion) {
        this.alphabet = alphabet
        this.zustaende = zustaende
        this.startzustand = startzustand
        this.endzustaende = endzustaende
        this.uebergangsFunktion = uebergangsFunktion
    }

    removeState(state) {
        this.uebergangsFunktion = removeUebergangsFunktionZustand(state, this.uebergangsFunktion)
        this.endzustaende = removeEndzustaendeZustand(state, this.endzustaende)
        this.startzustand = removeStartZustandZustand(state, this.startzustand)
        this.zustaende = removeZustaendeZustand(state, this.zustaende)
    }

    removeTerminal(terminal) {
        this.uebergangsFunktion = removeUebergangsFunktionTerminal(terminal, this.uebergangsFunktion)
        this.alphabet =this.removeAlphabetTerminal(terminal, this.alphabet)
    }

    findZielZustand(terminal, anfangsZustand) {
        this.uebergangsFunktion.findZielZustand(terminal, anfangsZustand)
    }

    complete() {
        console.log(dfa.uebergangsFunktion);
        for (let index = 0; index < this.alphabet.length; index++) {
            const t = this.alphabet[index];
            for (let index2 = 0; index2 < this.zustaende.length; index2++) {
                const z = this.zustaende[index2];
                if  (this.uebergangsFunktion.filter(u => u.terminal === t && u.anfangsZustand === z).length !== 1) {
                return false
                }
            }
            
        }
    return true
    }

    copy() {
        return new DFA(this.alphabet, this.zustaende, this.startzustand, this.endzustaende, this.uebergangsFunktion)
    }
}


let dfa = new DFA([],[],'',[],[])

class Uebergang {
/**
 * @param {string} terminal aus dem alphabet
 * @param {string} anfangsZustand aus dem zustanede
 * @param {string} zielZustand aus dem zustaende
 */
    constructor(terminal, anfangsZustand, zielZustand) {
        this.terminal = terminal
        this.anfangsZustand = anfangsZustand
        this.zielZustand = zielZustand
    }

    elementTerminal(terminal) {
        return this.terminal === terminal
    }
    findZielZustand(terminal, anfangsZustand) {
        this.find(e => e.terminal === terminal && e.anfangsZustand === anfangsZustand).zielZustand
    }
}
let bspDFA = new DFA(["b", "a"], ["z1", "z0", "z3"], "z0", ["z1"], [new Uebergang("a", "z0", "z1"), new Uebergang("b", "z0", "z0"), new Uebergang("a", "z1", "z1"), new Uebergang("b", "z1", "z0"), new Uebergang("a", "z3", "z0"), new Uebergang("b", "z3", "z0")]);

let bspDFA2 = new DFA(["b", "a"], ["z1", "z0"], "z0", ["z1"], [new Uebergang("b", "z0", "z1"), new Uebergang("a", "z0", "z0"), new Uebergang("a", "z1", "z1"), new Uebergang("b", "z1", "z0")]);

let bspDFA3 = new DFA(["a", "b", "c"], ["z0", "z1", "z2", "z3", "z4", "z5"], "z0", ["z4", "z5"], [new Uebergang("a", "z0", "z1"), new Uebergang("b", "z0", "z2"), new Uebergang("c", "z0", "z3"), new Uebergang("a", "z1", "z1"), new Uebergang("b", "z1", "z1"), new Uebergang("c", "z1", "z4"), new Uebergang("a", "z2", "z2"), new Uebergang("b", "z2", "z2"), new Uebergang("c", "z2", "z4"), new Uebergang("a", "z3", "z3"), new Uebergang("b", "z3", "z3"), new Uebergang("c", "z3", "z5"), new Uebergang("a", "z4", "z5"), new Uebergang("b", "z4", "z5"), new Uebergang("c", "z4", "z5"), new Uebergang("a", "z5", "z4"), new Uebergang("b", "z5", "z4"), new Uebergang("c", "z5", "z4")]);
// Eq

function eqArrays(array1, array2) {
    return (array1.length === array2.length && array1.every((value, index) => value === array2[index])) ? true : false
}

function eqDFA(dfa1, dfa2) {
    return (dfa1.startzustand === dfa2.startzustand && eqArrays(dfa1.alphabet, dfa2.alphabet) && eqArrays(dfa1.zustaende, dfa2.zustaende) && eqArrays(dfa1.endzustaende, dfa2.endzustaende) && (dfa1.uebergangsFunktion.length === dfa2.uebergangsFunktion.length) && (dfa1.uebergangsFunktion.every((value, index) => JSON.stringify(value) === JSON.stringify(dfa2.uebergangsFunktion[index])))) ? true : false
}

// Help Function

function removeChildren(parent) {
    while (parent.lastChild) {
        parent.removeChild(parent.lastChild)
    }
}

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }


//remove Functions
function removeUebergangsFunktionTerminal(terminal, uebergangsFunktion) {
    return uebergangsFunktion.filter(uebergang => uebergang.terminal !== terminal);
}

function removeAlphabetTerminal(eingabe, alphabet) {
    return alphabet.filter(terminal => terminal !== eingabe);
}

/**
 * 
 * @param {string} zustand 
 * @param {[Uebergang]} uebergangsFunktion 
 * @returns modified array of Uebergang where all uebergaenge that had the state as start or end point were removed
 */
function removeUebergangsFunktionZustand(zustand, uebergangsFunktion) {
    return uebergangsFunktion.filter(uebergang => uebergang.anfangsZustand !== zustand && uebergang.zielZustand !== zustand)
}

function removeEndzustaendeZustand(zustand, endzustaende) {
    return endzustaende.filter(endzustand => endzustand !== zustand)
}

function removeStartZustandZustand(zustand, startzustand) {
    return (zustand === startzustand ? undefined : startzustand)
}

function removeZustaendeZustand(eingabe, zustaende) {
    return zustaende.filter(zustand => zustand !== eingabe)
}


// AddText Menu

function title(text, menu) {
    let i = document.createElement('p')
    i.setAttribute('class', 'title')
    i.innerText = text
    menu.appendChild(i)
}

function subtitle(text, menu) {
    let i = document.createElement('p')
    i.setAttribute('class', 'subtitle')
    i.innerText = text
    menu.appendChild(i)
}

function warningText(text, referenceNode, buttonId) {
    let i = document.createElement('span')
    i.setAttribute('class', 'warning')
    i.innerText = '\nWarning:\n' + text
    insertAfter(referenceNode, i)
    document.getElementById(buttonId).disabled = true
    document.getElementById(buttonId).classList.add('timedout')

    setTimeout(removeWarning, 3000)
}

function removeWarning() {
    let disabled = document.querySelectorAll('.timedout')[0]
    disabled.disabled = false
    disabled.classList.remove('timedout')
    let elem = document.querySelector('.warning')
    elem.remove()
}

function stateInformation(id) {
    let element = document.getElementById(id)
    let iname = document.createElement('span')
    let istart = document.createElement('span')
    let iend = document.createElement('span')
    iname.setAttribute('class', 'menu-text')
    istart.setAttribute('class', 'menu-text')
    iend.setAttribute('class', 'menu-text')
    iname.innerText = ('name: ' + element.dataset.name)
    istart.innerText = ('\nstartstate: ')
    iend.innerText =('\nendstate: ')
    let lstart = document.createElement('input')
    let lend = document.createElement('input')
    lstart.setAttribute('type', 'checkbox')
    lend.setAttribute('type', 'checkbox')
    if (dfa.startzustand === id) {
        lstart.setAttribute('checked', 'true')
    }
    if (dfa.endzustaende.includes(id)) {
        lend.setAttribute('checked', 'true')
    }
    lstart.onclick = clickStartzustand
    lend.onclick = clickEndzustand
    leftMenu.appendChild(iname)
    leftMenu.appendChild(istart)
    leftMenu.appendChild(lstart)
    leftMenu.appendChild(iend)
    leftMenu.appendChild(lend)
}



function descriptonPlusInformation(description, information, menu) {
    let i = document.createElement('p')
    i.innerText = description + ': ' + information
    menu.appendChild(i)
}

function uebergangInformatin(id) {
    let uebergaenge = dfa.uebergangsFunktion.filter(e => e.anfangsZustand === id)
    let text = ''
    dfa.alphabet.forEach(element => {
        let existingUebergaenge = uebergaenge.filter(u => u.elementTerminal(element))
        if (existingUebergaenge.length >0){
            text = text + '\nterminal: ' + element + ', from: ' + id + ', to: ' + existingUebergaenge[0].zielZustand
        } else {
            text = text + '\nterminal: ' + element + ', from: ' + id + ', to: undefined' // Ziel zustand könnte auch undefined heißen!
        }
    })
    let i = document.createElement('p')
    i.setAttribute('class', 'menu-text')
    i.innerText = text
    leftMenu.appendChild(i)    
}

function overviewMenu() {
    removeChildren(leftMenu)
    title('DFA Info', leftMenu)
    descriptonPlusInformation('Alphabet', dfa.alphabet, leftMenu)
    let i = document.createElement('input')
    i.setAttribute('id', 'terminalInput')
    i.setAttribute('type', 'text')
    leftMenu.appendChild(i)
    let b1 = document.createElement('button')
    let b1tip = document.createElement('span')
    let b2 = document.createElement('button')
    let b2tip = document.createElement('span')
    b1.innerText = '+'
    b1.setAttribute('id', 'addTerminal')
    b1.setAttribute('class', 'tooltip')
    b2.innerText = '-'
    b1.onclick = clickAddTerminal
    b2.setAttribute('id', 'removeTerminal')
    b2.setAttribute('class', 'tooltip')
    b1tip.setAttribute('class', 'tooltiptext')
    b1tip.innerText = 'add a terminal'
    b2tip.setAttribute('class', 'tooltiptext')
    b2tip.innerText = 'remove a terminal'
    b2.onclick = clickRemoveTerminal
    leftMenu.appendChild(b1).appendChild(b1tip)
    leftMenu.appendChild(b2).appendChild(b2tip)
}


function rightInfomenu(text) {
    let b = document.createElement('button')
    b.innerText = 'Click me to show minimized automaton'
    b.setAttribute('id', 'minimizeAutomat')
    b.onclick = clickMinimizeButton
    rightMenu.appendChild(b)
}

rightInfomenu('')
/**
 * loads information about the state on the left when clicked
 * @param {string} id 
 */
function stateMenu(id) {
    removeChildren(leftMenu)
    title('State', leftMenu)
    stateInformation(id)
    if (dfa.alphabet.length === 1) {
        subtitle('transiton:', leftMenu)
        uebergangInformatin(id)
    } else if (dfa.alphabet.length > 1) {
        subtitle('transitons:', leftMenu)
        uebergangInformatin(id)       
    }
}

//Mouse Position

function positionXAxis(event, x) {
    let xCoordinate = event.clientX;
    let blackboardRect = document.getElementById('blackboard').getBoundingClientRect()

    switch (true) {
        case (xCoordinate < (blackboardRect.left+25)):
            return blackboardRect.left-x

        case (xCoordinate > (blackboardRect.right-25)):
            return blackboardRect.right-x-50;

        default:
            return xCoordinate-x-25
    }

}

function positionYAxis(event, y) {
    let yCoordinate = event.clientY;
    let blackboardRect = document.getElementById('blackboard').getBoundingClientRect()

    switch (true) {
        case (yCoordinate < (blackboardRect.top+25)):
            return blackboardRect.top-y;

        case (yCoordinate > (blackboardRect.bottom-25)):
            return blackboardRect.bottom-y-50;

        default:
            return yCoordinate-y-25
    }
}

//Rename function

function renameAlphabet(find, replace, dfa) {
    dfa.alphabet = dfa.alphabet.map(a => a === find ? replace : a);
    dfa.uebergangsFunktion.forEach(u => u.alphabet === find ? replace : u);
}

function renameZustand(find, replace, dfa) {
    let uefu = []
    for (let i = 0; i < dfa.uebergangsFunktion.length; i++) {
        uefu.push(renameZustandUebergang(find, replace, dfa.uebergangsFunktion[i]))
    }
    return new DFA(dfa.alphabet, dfa.zustaende.map(z => z===find?replace:z), dfa.startzustand===find?replace:dfa.startzustand, dfa.endzustaende.map(e => e===find?replace:e), uefu)
}

function renameZustandUebergang(find, replace, uF) {
    return new Uebergang(uF.terminal, uF.anfangsZustand===find?replace:uF.anfangsZustand, uF.zielZustand===find?replace:uF.zielZustand)

}

/**
 * 
 * @param {string} state 
 * @param {DFA} dfa 
 * @param {[string]} conectedStats 
 */
function findConectedStats(state, dfa, conectedStats) {
    for (let index = 0; index < dfa.uebergangsFunktion.length; index++) {
        let uebergang = dfa.uebergangsFunktion[index]
        if ((state === uebergang.anfangsZustand) && (conectedStats.indexOf(uebergang.zielZustand) === -1) && uebergang.zielZustand.includes('a')) {
            conectedStats.push(uebergang.zielZustand)
        }   
    }
}

function renameDFA(dfa) {
    let notRenamed = [dfa.startzustand]
    let count = 0
    while (notRenamed.length > 0) {
        dfa = renameZustand(notRenamed[0], 'x'+ count, dfa)
        notRenamed.shift()
        findConectedStats('x'+ count, dfa, notRenamed)
        count++
    }
    return dfa
}

// Sort Fuction
function sortStr(a, b) {
    switch (true) {
        case a < b: return -1
        case a > b: return 1
        default: return 0
    }
}


function sort2StrAtributes(a, b, c, d) {
    return sortStr(a,b) ? sortStr(a,b) : sortStr(c,d)
}

function sortAlphabet(dfa) {
    dfa.alphabet = dfa.alphabet.sort()
    dfa.uebergangsFunktion = dfa.uebergangsFunktion.sort((a, b) => sortStr(a.terminal, b.terminal))
}

function sortZustand(dfa) {
    dfa.zustaende = dfa.zustaende.sort()
    dfa.endzustaende =dfa.endzustaende.sort()
    dfa.uebergangsFunktion = dfa.uebergangsFunktion.sort((a,b) => sort2StrAtributes(a.anfangsZustand, b.anfangsZustand, a.zielZustand, b.zielZustand))
}

// Terminal/Start-/Endzustand

window.onclick = e => {
    if (e.target.className === 'zustand') {
        currentlyShownMenu = e.target.id
        stateMenu(e.target.id)
    } else if (e.target.id === 'blackboard') {
        currentlyShownMenu = 'Overview'
        overviewMenu()
    }
    else {
    }
} 

function terminalRadibutton() {
    return document.querySelector('input[name="selectedTerminal"]:checked').value
}

// Get State

function readAutomato() {
    let statsGUI = document.querySelectorAll('.zustand')
    let stats = []
    statsGUI.forEach(s => stats.push(s.getAttribute('id')))
    let endStats = []
    statsGUI.forEach(e => (e.getAttribute('data-startzustand')) ? endStats.push(e.getAttribute('id')) : {})
    let newStartzustand
    statsGUI.forEach(e => e.getAttribute('data-startzustand')? newStartzustand = e.getAttribute('id') : {})
    let tempDFA = new DFA (dfa.alphabet, stats, newStartzustand, endStats, dfa.uebergangsFunktion)
    return tempDFA
}

// Draw Uebergaenge

function addUebergang() {
    if (dfa.alphabet.length > 0) {
        uebergangPhase = 'first'
    }
    else {
        alert("You have to add a Terminal first!\nThen u can use the add Uebergang Button.")
        uebergangPhase = ''
    }
}

/**
 * adds a new arrow
 * @param {string} terminal 
 * @param {string} startID 
 * @param {sring} endID 
 * @param {int} startX 
 * @param {int} endX 
 */
function drawUebergang(terminal, startID, endID, startX, endX) {
    if (startX < endX) {
     let line1 = new LeaderLine(
        document.getElementById(startID),
        document.getElementById(endID),
        {middleLabel: LeaderLine.pathLabel(terminal), 
        color: 'white',
        id: startID + endID}
        )
    lineDrawn.push(line1)
    } else  {let line1 = new LeaderLine(
            document.getElementById(endID),
            document.getElementById(startID),
            {middleLabel: LeaderLine.pathLabel(terminal), 
            color: 'white',
            startPlug: 'arrow1',
            endPlug: 'behind'}
        )
    lineDrawn.push(line1)
    }
} //WIP

function redrawLine(id) {
    if (lineDrawn.length) {
        let linesToChange = lineDrawn.map(x => (x.start.id === id) || (x.end.id === id))
        let count = 0
        for (let index = 0; index < linesToChange.length; index++) {
            const element = lineDrawn[(index - count)]
            if (linesToChange[index]) {
                (element.endPlug === 'arrow1') ? drawUebergang(dfa.uebergangsFunktion.filter(u => u.anfangsZustand === element.start.id && u.zielZustand === element.end.id).map(u => u.terminal).join(", "), element.start.id, element.end.id, element.start.offsetLeft, element.end.offsetLeft) : drawUebergang(dfa.uebergangsFunktion.filter(u => u.anfangsZustand === element.end.id && u.zielZustand === element.start.id).map(u => u.terminal).join(", "), element.end.id, element.start.id, element.end.offsetLeft, element.start.offsetLeft) // make terminal dynamic
                element.remove()
                lineDrawn.splice((index - count), 1)
                count++
            }
        }
    }
}

function editUebergang(from, to) {
    let option = lineDrawn.find(l => l.start.id === from && l.end.id === to)
    if (option === undefined || option.startPlug === 'arrow1') {
    option = lineDrawn.find(l => l.start.id === to && l.end.id === from)
    }
    let path = dfa.uebergangsFunktion.filter(u => u.anfangsZustand === from && u.zielZustand === to).map(u => u.terminal).join(", ")
    option.middleLabel = LeaderLine.pathLabel(path)
    console.log(path);
}

function selfUebergang(something) {

}

// Add GUI Buttons

function addButtonGUI(id, text, parent) {
    let i = document.createElement('button')
    i.setAttribute('id', id)
    i.textContent = text
    parent.appendChild(i)
}

addButtonGUI('add-state','add-state', gameField)
const stateButton = document.querySelector('#add-state')

 function addUebergangGUI() {
    let i = document.createElement('div')
    i.setAttribute('id', 'expanding-uebergang')
    gameField.appendChild(i)
    const uebergangdiv = document.querySelector('#expanding-uebergang')
    let i2 = document.createElement('div')
    i2.setAttribute('id', 'terminal-selction')
    uebergangdiv.append(i2)
    addButtonGUI('uebergang', '', uebergangdiv)
    const uebergang = document.querySelector('#uebergang')
    uebergang.setAttribute('class', 'uebergang')
}

addUebergangGUI()
const uebergangDiv = document.querySelector('#expanding-uebergang')
const terminalSelction = document.querySelector('#terminal-selction')
const uebergangButton = document.querySelector('#uebergang')

function addTerminal() {
    removeChildren(terminalSelction)
    let count = 0
    let length = 50
    let div = document.createElement('div')
    div.setAttribute('id' ,'temporariTerminal')

    if (dfa.alphabet.length > 0) {for (let index = 0; index < dfa.alphabet.length; index++) {
        const element =  dfa.alphabet[index]
        let i = document.createElement('input')
        i.setAttribute('type', 'radio')
        i.setAttribute('id', ('terminal' + count))
        i.setAttribute('name', 'selectedTerminal')
        i.setAttribute('value', element)
        i.setAttribute('class', 'uebergang')
        terminalSelction.appendChild(i)
        length = length + 21.2

        let l = document.createElement('label')
        l.setAttribute('for', ('terminal' + count))
        l.setAttribute('id', 'terminalLabel' + count)
        count++
        l.innerText = element
        l.style.color = 'white'
        rightMenu.appendChild(l)
        length = length + l.offsetWidth + 1
        l.remove()
        terminalSelction.appendChild(l)
    }}
    div.remove()
    if (dfa.zustaende.length) {document.getElementById('terminal0').setAttribute('checked', 'true')}
    selectedTerminal = dfa.alphabet[0]
    document.getElementById('expanding-uebergang').style.setProperty('--dynamiUebergangSize', length + 'px');
}

addTerminal()

//Click Events

function clickState() {
    let identifier = 'x' + frischerZustand
    frischerZustand++
    // console.log(identifier)
    let i = document.createElement('img')
    i.setAttribute('src', '/src/images/zustand.png')
    i.setAttribute('data-name', identifier)
    i.setAttribute('data-startzustand', false)
    i.setAttribute('data-endzustand', false)
    i.setAttribute('alt', 'state')
    i.setAttribute('id', identifier)
    i.setAttribute('class', 'zustand')
    i.style.position = 'relative';
    // Draggable
    gameField.appendChild(i)
    dfa.zustaende.push(identifier)
    i.setAttribute('x', i.getBoundingClientRect().x)
    i.setAttribute('y', i.getBoundingClientRect().y)
    i.addEventListener("dragend", function(event) {
        
        let mX = positionXAxis(event, i.getAttribute('x'))
        i.style.left = `${mX}px`
        let mY = positionYAxis(event, i.getAttribute('y'))
        i.style.top = `${mY}px`
        redrawLine(this.id)
    })
    i.addEventListener('click',  function() {
        switch (uebergangPhase) {
            case 'first':
                lastStateClicked = this
                uebergangPhase = 'second'
                break;

            case 'second':
                uebergangPhase = ''
                dfa.uebergangsFunktion.push(new Uebergang (terminalRadibutton(), lastStateClicked.id, this.id))
                dfa.uebergangsFunktion.sort((a, b) => sortStr(a.terminal, b.terminal))
                console.log(dfa.uebergangsFunktion);
                if(lastStateClicked.id === this.id) {
                    console.log("arrow to self");
                    selfUebergang(this)
                } else if(dfa.uebergangsFunktion.some(u => u.anfangsZustand === lastStateClicked.id && u.zielZustand === this.id && u.terminal !== terminalRadibutton())) {
                    console.log("arrow already exists");
                    editUebergang(lastStateClicked.id, this.id)
                } else {
                    console.log("new arrow");
                drawUebergang(terminalRadibutton(), lastStateClicked.id, this.id, lastStateClicked.offsetLeft, this.offsetLeft)}
                break;

            default:
                break;
        }
    })
    readAutomato()
}

function clickAddTerminal() {
    let append = document.getElementById('removeTerminal')
    let text = document.getElementById('terminalInput').value
    if (text !== '' && !dfa.alphabet.includes(text)) {
        dfa.alphabet.push(text)
        dfa.alphabet.sort()
        overviewMenu()
        addTerminal()
    } else if (text === '') {
        warningText('No input detected', append, 'addTerminal')
    } else {
        warningText(`${text} is already a terminal`, append, 'addTerminal')
    }
}

function clickRemoveTerminal() {
    let append = document.getElementById('removeTerminal')
    let text = document.getElementById('terminalInput').value
    if (text !== '' && dfa.alphabet.includes(text)) {
        dfa.alphabet.splice(dfa.alphabet.indexOf(text), 1)
        overviewMenu()
        addTerminal()
    } else if (text === '') {
        warningText('No input detected', append, 'removeTerminal')
    } else {
        warningText(`${text} is not a terminal`, append, 'removeTerminal')
    }
}

function clickMinimizeButton() {
    let d = dfa.copy()
    dfa.complete() ? console.log(minimizeAutomato(removeUnreachabelStats(d))) : warningText('the dfa is incomplete. Please make sure every state has a transiton defined for every terminal',this,this.id)
}

function clickStartzustand() {
    if (this.checked) {
        dfa.startzustand = currentlyShownMenu
        updateStateGUI(currentlyShownMenu)
    } else {
        dfa.startzustand = ''
        updateStateGUI(currentlyShownMenu)
    }
}

function clickEndzustand() {
    if (this.checked) {
        if (!dfa.endzustaende.includes(currentlyShownMenu)) {
            dfa.endzustaende.push(currentlyShownMenu)
            dfa.endzustaende.sort()
        }
        updateStateGUI(currentlyShownMenu)
    } else {
        if (dfa.endzustaende.includes(currentlyShownMenu)) {
            dfa.endzustaende.splice(dfa.endzustaende.indexOf(currentlyShownMenu), 1)
        }
        updateStateGUI(currentlyShownMenu)
    }
}

function updateStateGUI(id) {
    let state = document.getElementById(id)
    switch (true) {
        case dfa.startzustand === id && dfa.endzustaende.includes(id):
            state.setAttribute('src', '/src/images/startzustand-endzustand.png')
            state.setAttribute('alt', 'start- and endstate')
            break;

        case dfa.startzustand === id:
            state.setAttribute('src', '/src/images/startzustand.png')
            state.setAttribute('alt', 'startstate')
            break;

        case dfa.endzustaende.includes(id):
            state.setAttribute('src', '/src/images/endzustand.png')
            state.setAttribute('alt', 'endstate')
            break;
                
        default:
            state.setAttribute('src', '/src/images/zustand.png')
            state.setAttribute('alt', 'state')
    }
}

stateButton.onclick = clickState

uebergangButton.onclick = addUebergang

//Entferne unereichabre Zustände

function removeUnreachabelStats(eingabe) {
    let reachable = []
    let stillToTest = []
    if (eingabe.startzustand !== '') {
        stillToTest.push(eingabe.startzustand)
    }
    while (stillToTest.length) {

        if (!reachable.includes(stillToTest[0])) {
            reachable.push(stillToTest[0])
            stillToTest = stillToTest.concat(eingabe.uebergangsFunktion.filter(e => e.anfangsZustand === stillToTest[0]).map(e => e.zielZustand))
            stillToTest.splice(0, 1)
        } else {
            stillToTest.splice(0, 1)
        }
    }
    let toRemove = eingabe.zustaende.filter(e => !reachable.includes(e))
    toRemove.forEach(e => eingabe.removeState(e))
    return eingabe
}

// Automaten Minimierung

function minimizeAutomato(automat) {
    // Markieren von nicht Endzuständen und Endzuständen
    let array1 = []
    automat.zustaende.forEach(e1 => array1.push([e1, automat.zustaende.filter(e2 => e2 !== e1)]))
    array1.forEach(e1 => (automat.endzustaende.includes(e1[0])) ? e1[1] = e1[1].filter(e2 => automat.endzustaende.includes(e2)) : e1[1] = e1[1].filter(e2 => !automat.endzustaende.includes(e2)))
    oldArray = []
    // Schleife des Minimierungs-Algorithmus
    while (oldArray !== array1) {
        oldArray = array1
        for (let elem of oldArray){
            let zust = elem[0]
            let listZust = elem[1]
            for (let zust2 of listZust) {
                for (let terminal of automat.alphabet) {
                    let zielZustand1 = automat.uebergangsFunktion.find(u => u.terminal === terminal && u.anfangsZustand === zust).zielZustand
                    let zielZustand2 = automat.uebergangsFunktion.find(u => u.terminal === terminal && u.anfangsZustand === zust2).zielZustand
                    if(zielZustand1 !== zielZustand2 && ! oldArray.find(l => l[0] === zielZustand1)[1].includes(zielZustand2)) {
                        array1[array1.findIndex(e3 => e3[0] === zust)][1] = array1[array1.findIndex(e4 => e4[0] === zust)][1].filter(e5 => e5 !== zust2)
                        array1[array1.findIndex(e3 => e3[0] === zust2)][1] = array1[array1.findIndex(e4 => e4[0] === zust2)][1].filter(e5 => e5 !== zust) // WORK
                    }
                }

            }
        } 
    }
    // Bestimme weclhe zusände Gelöscht werden müssen
    let toRemove = []
    for (let index = 0; index < array1.length; index++) {
        const element = array1[index]
        toRemove = toRemove.concat(element[1])
        element[1].forEach( e1 => array1 = array1.filter(e2 => e2[0] !== e1))
    }
    // Minimizing Automat
    let copyAutomat = automat
    copyAutomat.uebergangsFunktion.forEach(e1 => (toRemove.includes(e1.zielZustand)) ? e1.zielZustand = array1.find(e2 => e2[1].includes(e1.zielZustand))[0] : e1.zielZustand = e1.zielZustand)
    toRemove.forEach(e => copyAutomat.removeState(e))
    return copyAutomat
}
// [ ['z0', ['z1', 'z2']] ,]
minimizeAutomato(bspDFA3)

//DFA Vergleich

function compareDFA(firstDFA, secondDFA) {
    let dfa1 = firstDFA
    for (let ter of dfa1.zustaende) {
        dfa1 = renameZustand(ter, ter + "a", dfa1)
    }
    sortAlphabet(dfa1)
    dfa1 = renameDFA(dfa1)
    sortZustand(dfa1)      
    let dfa2 = secondDFA
    for (let ter of dfa2.zustaende) {
        dfa2 = renameZustand(ter, ter + "a", dfa2)
    }
    sortAlphabet(dfa2)
    dfa2 = renameDFA(dfa2)
    sortZustand(dfa2)
}


//get JSON (async)

async function getDFAJSON () {
    let jsonObj = await fetch('./dfa.json')
    return dfaJson = await jsonObj.json()
}

getDFAJSON().then (dfaJson => compareDFA(dfaJson.dfa1, bspDFA))



// Verständnis Bsp.: (Wörter der länge |w| = 2 oder endet auf a)
let bspAlphabet = ["a","b"]
let bspZustaende = ["x0","x1","x2","x3","x4"]
let bspStartzustand = "x0"
let bspEndzustaende = ["x1","x3"]
let bspuebergangsfunktion = [new Uebergang("a","x0","x1"),new Uebergang("b","x0","x2"),new Uebergang("a","x1","x3"),new Uebergang("b","x1","x3"),new Uebergang("a","x2","x3"),new Uebergang("b","x2","x3"),new Uebergang("a","x3","x3"),new Uebergang("b","x3","x4"),new Uebergang("a","x4","x3"),new Uebergang("b","x4","x4")]


//alert("WE HAVE A PROBLEM AHHHHHHHHHHHHHHHHH!!!!!!!!!!!!!1")

//Json um automaten darzustellen + Multipel Choice
//Json biblotheken
//30-70 Seiten Einleitungen -> Grundlagen (Automaten, JS) -> Hauptteil (mehere Kapitel [entwurf (abstrakt kein code), umsetzung (einzelne code beispiele), testen (vorführen)])-> Schluss (zusammenfassung (1/2 Seite) + ausblick/erweiterung)
//erstmal auf DFA Beschrenken

